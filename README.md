Cloudify Helm Plugin
====================


## Tests

To run the example plugin tests, the included `dev-requirements.txt` should be installed.

```
pip install -r dev-requirements.txt
```
