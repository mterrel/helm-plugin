########
# Copyright (c) 2017 Unbounded Systems, LLC. All rights reserved
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
#    * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    * See the License for the specific language governing permissions and
#    * limitations under the License.


import os.path
import subprocess
import sys

from cloudify import ctx as _ctx
from cloudify.decorators import operation
from cloudify.exceptions import RecoverableError, NonRecoverableError
from cloudify.utils import exception_to_error_cause


# TODO: Helm container should be stored in a better repo
# TODO: Possibly move to helm API or use k8s directly instead
HELM_IMAGE = 'mterrel/helm:latest'
KUBECTL_IMAGE = 'mterrel/kubectl:latest'

REL_MANAGED_BY_KUBERNETES = 'ubs.helm.relationships.managed_by_kubernetes'

SA_CREATED_BY = 'sa_created_by'
SERVICE_ACCOUNT = 'service_account'
NAMESPACE = 'namespace'

CLI_CONTAINER_HOMEDIR = '/config'


@operation
def create(**kwargs):
    _ctx.logger.info('Helm: starting create')
    Helm(_ctx).create()
    _ctx.logger.info('Helm: create successfully completed')


@operation
def delete(**kwargs):
    _ctx.logger.info('Helm: starting delete')
    Helm(_ctx).delete()
    _ctx.logger.info('Helm: delete successfully completed')


class KubeConfig(object):
    def __init__(self, ctx, contents):
        self.contents = contents
        self.filename = os.path.join(ctx.plugin.workdir, 'kubectl.config')

    def write_file(self):
        try:
            f = open(self.filename, 'w')
            f.write(self.contents)
        finally:
            f.close()


class Helm(object):
    def __init__(self, ctx):
        self.ctx = ctx
        self._service_account = None
        self._kube_config = None

    @property
    def namespace(self):
        return self.ctx.node.properties[NAMESPACE]

    @property
    def service_account_name(self):
        return self.ctx.node.properties[SERVICE_ACCOUNT]

    @property
    def kube_config(self):
        if not self._kube_config:
            self._kube_config = KubeConfig(self.ctx, self.get_kube_config())
        return self._kube_config

    @property
    def service_account(self):
        if not self._service_account:
            try:
                self._service_account = ServiceAccount(
                        self.service_account_name, self.namespace,
                        self.kube_config, self.instance.runtime_properties)
            except KeyError:
                raise NonRecoverableError(
                        'Helm: service_account and namespace must be ' \
                        'specified on a Helm node')
        return self._service_account

    @property
    def instance(self):
        return self.ctx.instance

    def get_kube_config(self):
        for rel in self.instance.relationships:
            if rel.type == REL_MANAGED_BY_KUBERNETES:
                cfg = None
                try:
                    kub_props = rel.target.instance.runtime_properties
                    cfg = kub_props['deployment']['outputs']['kubectl_config']
                except:
                    pass
                if cfg:
                    return cfg
                raise NonRecoverableError(
                    'Helm: Could not find a %s relationship target with '
                    'a populated kubectl_config property' % \
                    REL_MANAGED_BY_KUBERNETES)
        raise NonRecoverableError(
            'Helm: A Helm node is required to have a relationship of type %s' \
            % REL_MANAGED_BY_KUBERNETES)

    def create(self):
        check_docker_ready()

        self.kube_config.write_file()

        self.service_account.create()

        try:
            helm_cmd = 'init --tiller-namespace=%s' % self.namespace
            if self.service_account.use:
                helm_cmd += ' --service-account=%s' % self.service_account.name
            self.cli(helm_cmd)
        except:
            self.service_account.delete()
            _, ex, traceback = sys.exc_info()
            raise NonRecoverableError(
                    'Helm create: Error installing tiller on cluster',
                    causes=[exception_to_error_cause(ex, traceback)])

    def delete(self):
        ex_list = ()
        traceback_list = ()
        try:
            self.cli('reset')
        except:
            _, ex, traceback = sys.exc_info()
            ex_list.push(ex)
            traceback_list.push(traceback)

        try:
            self.service_account.delete()
        except:
            _, ex, traceback = sys.exc_info()
            ex_list.push(ex)
            traceback_list.push(traceback)
        if len(ex_list) == 0:
            return
        if len(ex_list) == 1:
            msg = ('Helm delete: Error while uninstalling helm and '
                   'removing service account')
        else:
            msg = ('Helm delete: %d errors while uninstalling helm and '
                   'removing service account: ') + \
                  ', '.join([ repr(e) for e in ex_list ])
            
        raise NonRecoverableError(msg,
                causes=[exception_to_error_cause(ex[0], traceback[0])])


    def cli(self, arg_str):
        run_container_cmd(HELM_IMAGE, arg_str, self.kube_config)


class Kubectl(object):
    def __init__(self, kube_config):
        self.kube_config = kube_config

    def cli(self, arg_str):
        run_container_cmd(KUBECTL_IMAGE, arg_str, self.kube_config)


class ServiceAccount(object):
    def __init__(self, name, namespace, kube_config, runtime_props):
        self.name = name
        self.namespace = namespace
        self.kubectl = Kubectl(kube_config)
        # Where we should get/keep any runtime properties
        self.runtime_props = runtime_props

        # Should we even use a service account?
        self.use = (self.name != 'NONE')

    def create(self):
        if not self.use:
            return

        try:
            self.kubectl.cli('create sa %s --namespace=%s' % (self.name,
                                                              self.namespace))
            # Remember it was us who created it for later deletion
            self.runtime_props[SA_CREATED_BY] = _ctx.deployment.id
            return

        except subprocess.CalledProcessError as e:
            # Error 'AlreadyExists' is one we're expecting. Anything else is
            # actually an error.
            _ctx.logger.error('ServiceAccount kubectl err: %s' % e.output)
            if 'AlreadyExists' in e.output: pass
            else:
                _, ex, traceback = sys.exc_info()
                raise NonRecoverableError(
                        'Helm create: Error creating service account',
                        causes=[exception_to_error_cause(ex, traceback)])

    def delete(self):
        if not self.use:
            return
        # Did we create this or did someone else?
        if self.runtime_props.get(SA_CREATED_BY, None) == _ctx.deployment.id:
            self.kubectl.cli('delete sa %s --namespace=%s' % (self.name,
                                                              self.namespace))
            del self.runtime_props[SA_CREATED_BY]


def run_cmd(cmd_arr):
    command = ' '.join(cmd_arr) # for debugging
    _ctx.logger.debug('command: {0} '.format(command))
    try:
        output = subprocess.check_output(cmd_arr, stderr=subprocess.STDOUT)
        _ctx.logger.debug('output: {0} '.format(output))
        return output

    except subprocess.CalledProcessError as e:
        _ctx.logger.debug('output: {0} '.format(e.output))
        _ctx.logger.debug('Running `%s` returned error (%d)' % (command,
                                                                e.returncode))
        raise


def check_docker_ready():
    try:
        run_cmd('docker ps'.split())
    except Exception:
        _, ex, traceback = sys.exc_info()
        raise NonRecoverableError('Helm create: docker not installed and ready',
                causes=[exception_to_error_cause(ex, traceback)])


def run_container_cmd(image, arg_str, kube_config):
    cmd = 'docker run --rm -v %s:%s/.kube/config %s %s' % (
            kube_config.filename, CLI_CONTAINER_HOMEDIR, image, arg_str)
    run_cmd(cmd.split())

