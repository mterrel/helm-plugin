PLUGIN_NAME:=helm-plugin
DIST:=dist

upload: delete wagon
	cfy plugin upload `ls $(DIST)/*wgn`

wagon: clean_dist
	wagon create --format tar.gz -o $(DIST) $(CURDIR)

delete:
	ID=`cfy plugin list | grep $(PLUGIN_NAME) | awk '{print $$2}'` \
	&& if [ -n "$$ID" ]; then cfy plugin delete "$$ID"; fi

clean_dist:
	rm -rf $(DIST)
